import {
    TxContext,
    NewTx,
    getContractId,
    continueTx,
    passCwebFrom,
    contractIssuer,
    store,
    getMethodArguments,
    genericClaim,
    claimKey,
    toHex,
    addDefaultMethodHandler,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    getContextTx,
    call,
    read,
    dataUnverified,
    getContextCall,
    isSelfCall,
    getContractArgument,
    executeHandler,
    isResolvedRead,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

const CALLBACK_FOR_READOP = "CALLBACK";

function logic(contextTx: TxContext): NewTx[] {
    const self = getContractId(contextTx);
    const issuer = getMethodArguments(contextTx)[1].issuer;
    const key = getMethodArguments(contextTx)[1].key;
    console.log("Issuer for ReadOp: ", JSON.stringify(issuer));
    console.log("Key for ReadOp: ", JSON.stringify(key));
    return [
        continueTx([
            passCwebFrom(contractIssuer(self), 600),
            call(2, self),
            dataUnverified([CALLBACK_FOR_READOP]),
            read(issuer, key),
        ]),
    ];
}

function callback(contextTx: TxContext): NewTx[] {
    const self = getContractId(contextTx);
    const contextCall = getContextCall();
    if (!isSelfCall(contextTx, contextCall)) {
        throw "Only contract itself can call it";
    }
    // TODO: add extract method for ReadOp
    const readop = getContractArgument(contextTx, 2);
    if (!isResolvedRead(readop)) {
        throw "Incorrect provided op for callback method. Expected ReadOp";
    }
    console.log("ReadOp", JSON.stringify(readop));
    return [
        continueTx([
            passCwebFrom(contractIssuer(self), 200),
            store(
                genericClaim(
                    claimKey("Some awesome key first", "Some awesome key second"),
                    readop.ReadOp.results[0].content.body,
                    toHex(0),
                ),
            ),
        ]),
    ];
}

export function cwebMain() {
    addDefaultMethodHandler(logic);
    addMethodHandler(CALLBACK_FOR_READOP, callback);
    addMethodHandler(SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    const contextTx = getContextTx();
    executeHandler(contextTx);
}
